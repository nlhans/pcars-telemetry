namespace pCarsTelemetry
{
    public class Conversions
    {
        public const float FuelLitreToKg = 0.7489f;
        public const float FuelKgToLitre = 1/0.7489f;

        public const float AccelerationMpspsToG = 1/9.81f;

        public const float PowerkWtoHp =1/0.73549875f;
    }
}