using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using pCarsTelemetry.API;

namespace pCarsTelemetry
{
    public class InteriaMeasureTool
    {
        private Dictionary<float, float> speed2deacc = new Dictionary<float, float>();

        private bool isMeasuring = false;

        public void Export()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var kvp in speed2deacc)
            {
                sb.Append(kvp.Key + "," + kvp.Value + "\n");
            }

            File.WriteAllText("./inertiatest.csv", sb.ToString());
        }

        public void Tick(PcarsTelemetry obj)
        {
            if (!isMeasuring)
            {
                if (obj.mUnfilteredThrottle <= 0.05 && obj.mClutch >= 0.95 && obj.mBrake <= 0.01)
                {
                    isMeasuring = true;
                }
            }
            else if (isMeasuring)
            {
                if (obj.mUnfilteredThrottle >= 0.05 || obj.mClutch <= 0.95 || obj.mBrake >= 0.01 || Math.Abs(obj.mSteering) > 0.1)
                {
                    isMeasuring = false;
                }
                else
                {
                    var speedMs10s = (float) Math.Round(obj.mSpeed*10);

                    if (speed2deacc.ContainsKey(speedMs10s))
                        speed2deacc[speedMs10s] = obj.mLocalAcceleration[2];
                    else
                        speed2deacc.Add(speedMs10s, obj.mLocalAcceleration[2]);
                }
            }
        }
    }
}