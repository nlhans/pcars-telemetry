﻿namespace pCarsTelemetry
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblLF = new System.Windows.Forms.Label();
            this.lblRF = new System.Windows.Forms.Label();
            this.lblLR = new System.Windows.Forms.Label();
            this.lblRR = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // lblLF
            // 
            this.lblLF.AutoSize = true;
            this.lblLF.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLF.Location = new System.Drawing.Point(407, 284);
            this.lblLF.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblLF.Name = "lblLF";
            this.lblLF.Size = new System.Drawing.Size(32, 24);
            this.lblLF.TabIndex = 1;
            this.lblLF.Text = "LF";
            // 
            // lblRF
            // 
            this.lblRF.AutoSize = true;
            this.lblRF.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRF.Location = new System.Drawing.Point(706, 284);
            this.lblRF.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblRF.Name = "lblRF";
            this.lblRF.Size = new System.Drawing.Size(35, 24);
            this.lblRF.TabIndex = 2;
            this.lblRF.Text = "RF";
            this.lblRF.Click += new System.EventHandler(this.lblRF_Click);
            // 
            // lblLR
            // 
            this.lblLR.AutoSize = true;
            this.lblLR.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLR.Location = new System.Drawing.Point(407, 414);
            this.lblLR.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblLR.Name = "lblLR";
            this.lblLR.Size = new System.Drawing.Size(33, 24);
            this.lblLR.TabIndex = 3;
            this.lblLR.Text = "LR";
            // 
            // lblRR
            // 
            this.lblRR.AutoSize = true;
            this.lblRR.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRR.Location = new System.Drawing.Point(706, 414);
            this.lblRR.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblRR.Name = "lblRR";
            this.lblRR.Size = new System.Drawing.Size(36, 24);
            this.lblRR.TabIndex = 4;
            this.lblRR.Text = "RR";
            this.lblRR.Click += new System.EventHandler(this.lblRR_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1588, 1054);
            this.Controls.Add(this.lblRR);
            this.Controls.Add(this.lblLR);
            this.Controls.Add(this.lblRF);
            this.Controls.Add(this.lblLF);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblLF;
        private System.Windows.Forms.Label lblRF;
        private System.Windows.Forms.Label lblLR;
        private System.Windows.Forms.Label lblRR;
    }
}