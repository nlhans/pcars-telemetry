﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Documents;
using System.Windows.Forms;
using pCarsTelemetry.API;
using Timer = System.Windows.Forms.Timer;

namespace pCarsTelemetry
{
    public partial class MainForm : Form
    {
        private float lastMaxRpm = 0.0f;
        private float lastLap = 0xFFFFFF;
        private float lastFuelCount = 100;
        private List<float> FuelUsage = new List<float>();
        private SerialPort sp;
        private SharedMemory<PcarsTelemetry> _mmf;
        private InteriaMeasureTool imt;
        public MainForm()
        {
            InitializeComponent();

            sp = new SerialPort("COM4",115200);
            sp.Open();

            _mmf = new SharedMemory<PcarsTelemetry>("$pcars$");
            imt = new InteriaMeasureTool();
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);

            this.Closing += (sender, args) => imt.Export();

            var t = new Timer {Interval = 10};
            t.Tick += updateTelemetry;
            t.Start();
        }

        private void InitGauges(float maxRpm)
        {
            for (int led = 1; led <= 16; led++)
            {
                sp.Write("led-animation " + led + ",-1 5000,0,5000 2000,4000\r\n");
                System.Threading.Thread.Sleep(10);
            }
            for (int led = 1; led <= 16; led++)
            {
                sp.Write("led-animation " + led + ",0 0,5000,0 2000,4000\r\n");
                System.Threading.Thread.Sleep(10);
            }
            float gear2gearRatio = 0.9f;
            maxRpm -= 100;
            for (int gear = 1; gear < 10; gear++)
            {
                for (int led = 1; led <= 16; led++)
                {
                    Color c = RainbowColor.HsvToRgb(led / 16.0f * 360, 1.0f, 1.0f);
                    var r = c.R * 10;
                    var g = c.G * 10;
                    var b = c.B * 10;

                    var baseRpm = gear2gearRatio*maxRpm;
                    var addRpm = maxRpm - baseRpm;
                    var rpm = baseRpm + addRpm / 15.0f * (led - 1);

                    var s = rpm - 50;
                    var e = rpm;
                    if (s < 0) s = 0;
                    if (e > maxRpm-50) e = maxRpm-50;
                    sp.Write("led-animation " + led + "," + gear + " " + r + "," + g + "," + b + " " + s + "," + e + "\r\n");
                    System.Threading.Thread.Sleep(10);
                }
            }
        }

        private float avgDiff = 0.0f;
        private float slipCounter = 0;

        void updateTelemetry(object sender, EventArgs e)
        {
            var obj = _mmf.Read();
            imt.Tick(obj);
            
            // 1490 is estimated kerb weight of McLaren P1 car.
            var CarWeight = 1490 + Conversions.FuelLitreToKg * obj.mFuelLevel; // kg
            var Acceleration = -obj.mLocalAcceleration[2] ; // m/s^2
            var Velocity = obj.mSpeed;

            var wheelSpeed = obj.mTyreRPS[3]*Math.PI*2;

            // a = F/m
            // F = m*a
            // P = F*v
            // F is in theory power, but in this simple case we assume straight line testing
            // -> P=m*a*v = 

            var WheelPowerW = CarWeight * Acceleration * Velocity;
            var WheelPowerHp = WheelPowerW/1000.0f*Conversions.PowerkWtoHp;


            try
            {
                label1.Text = string.Format("{0:0000.000}SPD {1:0000.000}HP ENGINE: {2:000.00}% AERO {3:000.0000}%",
                    wheelSpeed, WheelPowerHp, obj.mEngineDamage*100, obj.mAeroDamage*100);

                label1.Text = label1.Text + "\nAmbient temp: " + obj.mAmbientTemperature.ToString("000.00") + "C";
                label1.Text = label1.Text + "\nTrack temp: " + obj.mTrackTemperature.ToString("000.00") + "C";
                label1.Text = label1.Text + "\nOil temp: " + obj.mOilTempCelsius.ToString("000.00") + "C";

                label1.Text = label1.Text + "\nRAIN RAIN: " + (obj.mRainDensity*100).ToString("000.00") + "C";

                label1.Text = label1.Text + "\nTERRAIN: " + obj.mTerrain[0].ToString("000.00") + ", " +
                              obj.mTerrain[1].ToString("000.00") + ", " + obj.mTerrain[2].ToString("000.00") + ", " +
                              obj.mTerrain[3].ToString("000.00");
            }
            catch
            {
            }
            try
            {
                if (obj.mLastLapTime != lastLap)
                {
                    FuelUsage.Add(obj.mFuelLevel*obj.mFuelCapacity - lastFuelCount);
                    lastFuelCount = obj.mFuelLevel*obj.mFuelCapacity;
                    lastLap = obj.mLastLapTime;
                }

                while (FuelUsage.Count >= 15)
                    FuelUsage.RemoveAt(0);
            }
            catch
            {
            }
            try{
            label1.Text = label1.Text + "\n\nFuel usage history:\n\n" +
                              string.Join("\r\n", FuelUsage.Select(x => x.ToString("0.00L")));



                this.lblLF.Text = "T-" + obj.mTyreTemp[0].ToString("000") + "C (" + (obj.mTyreWear[0] * 100).ToString("000.00") + "%)\n" +
                                  "B-" + obj.mBrakeTempCelsius[0].ToString("000") + "C (" + (obj.mBrakeDamage[0] * 100) + "%)\n" +
                                  "SD-"+(obj.mSuspensionDamage[0]*100).ToString("000.00");
                this.lblRF.Text = "T-" + obj.mTyreTemp[1].ToString("000") + "C (" + (obj.mTyreWear[1] * 100).ToString("000.00") + "%)\n" +
                                  "B-" + obj.mBrakeTempCelsius[1].ToString("000") + "C (" + (obj.mBrakeDamage[1] * 100) + "%)\n" +
                                  "SD-" + (obj.mSuspensionDamage[1] * 100).ToString("000.00");
                this.lblLR.Text = "T-" + obj.mTyreTemp[2].ToString("000") + "C (" + (obj.mTyreWear[2] * 100).ToString("000.00") + "%)\n" +
                                  "B-" + obj.mBrakeTempCelsius[2].ToString("000") + "C (" + (obj.mBrakeDamage[2] * 100) + "%)\n" +
                                  "SD-" + (obj.mSuspensionDamage[2] * 100).ToString("000.00");
                this.lblRR.Text = "T-" + obj.mTyreTemp[3].ToString("000") + "C (" + (obj.mTyreWear[3] * 100).ToString("000.00") + "%)\n" +
                                  "B-" + obj.mBrakeTempCelsius[3].ToString("000") + "C (" + (obj.mBrakeDamage[3] * 100) + "%)\n" +
                                  "SD-" + (obj.mSuspensionDamage[3] * 100).ToString("000.00");
            }
            catch (Exception ex)
            {

            }

            /*** RACE DASHBOARD ***/
            ProcessDashboard(obj);
        }

        private void ProcessDashboard(PcarsTelemetry obj)
        {
//obj.mMaxRPM = 8250; // LMP1
            //obj.mMaxRPM = 12250;
            if (obj.mMaxRPM != lastMaxRpm)
            {
                InitGauges(obj.mMaxRPM);
                lastMaxRpm = obj.mMaxRPM;
            }
            obj.mMaxRPM = 10250;
            var avgFSlip = Math.Abs(obj.mTyreSlipSpeed[0] + obj.mTyreSlipSpeed[1])/2;
            var avgRSlip = Math.Abs(obj.mTyreSlipSpeed[2] + obj.mTyreSlipSpeed[3])/2;

            var diff = Math.Abs(avgFSlip - avgRSlip);
            avgDiff = diff*0.5f + avgDiff*0.5f;
            if (slipCounter == 10)
            {
                if (avgFSlip > avgRSlip)
                {
                    var intensity = (diff - 1)/5.0f;
                    var rV = 1 + intensity*25;
                    if (rV > 25) rV = 0;
                    if (rV < 0) rV = 0;
                    sp.Write("i " + Math.Round(rV) + " 0 0\r\n");
                }
                else
                {
                    var intensity = (diff - 1)/5.0f;
                    var rB = 1 + intensity*15;
                    if (rB > 15) rB = 0;
                    if (rB < 0) rB = 0;
                    sp.Write("i 0 0 " + Math.Round(rB) + "\r\n");
                }
                Thread.Sleep(5);
                slipCounter = 0;
            }
            else
            {
                slipCounter++;
            }
            sp.Write("rpm " + Math.Round(obj.mRPM) + "\r\n");
            sp.Write("gear " + obj.mGear + "\r\n");
        }

        private void lblRF_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void lblRR_Click(object sender, EventArgs e)
        {

        }
    }
}
