﻿using System;
using System.Windows.Forms;

namespace pCarsTelemetry
{
    public static class ProgramEntry
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
